import Nav from './Nav';
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import React from 'react';
import MainPage from './MainPage';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';


function App(props) {
  return (

    <React.Fragment>
    <div className="container">
    <BrowserRouter>
         {<Nav />}
          <Routes>
          <Route index element={<MainPage />} />
          <Route path="attendees" element={<AttendeesList />} />
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm  />} />
          </Route>
          <Route path="location" element={<LocationForm  />} />
          <Route path="conferences/new" element={<ConferenceForm  />} />
          <Route path="presentations" element={<PresentationForm  />} />
        </Routes>
      </BrowserRouter>
    </div>
    </React.Fragment>
  );
}

export default App;
