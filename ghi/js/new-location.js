window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/'

    const response = await fetch(url)

    if (response.ok) {
        const data = await response.json();
        const selector = document.getElementById('state')
        for (let state of data.states) {
            const stateValue = document.createElement('option')
            console.log(stateValue)
            stateValue.value = state.abbreviation
            console.log(stateValue.value)
            stateValue.innerHTML = state.name
            console.log(stateValue.innerHTML)
            selector.appendChild(stateValue)
        }
        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            try {
                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newLocation = await response.json();
                    console.log(newLocation);
                }
            } catch (err) {
                console.log(err)
            }
        })
    }
})
